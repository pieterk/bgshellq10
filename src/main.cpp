/*  Copyright (C) 2008 e_k (e_k@users.sourceforge.net)

    Ported to Blackberry Playbook by BGmot <support@tm-k.com>, 2012

    This application is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This application is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301, USA.
*/


#include <QtCore>
#include <QtGui>
#include <QtGui/QApplication>
#include <QTextCodec>
#include <fcntl.h>
#include "qtermwidget.h"
#include "mymenu.h"
#include "mysystemmenu.h"
#include "mymainwindow.h"
#include "myvk.h"

int masterFdG = -1; // will be used in parent process
int slaveFdG = -1;  // will be used in child process
int pidG_ = -1;     // child's PID
QTermWidget *console; // our 'main' widget, let's make it global so it is available in Menu widget
CMyMainWindow *mainWindow;
CMyMenu *Menu;      // Menu with soft buttons
CMySystemMenu *SystemMenu; // System menu from 'swipe down' event
QFont font;
bool bCtrlFlag = false; // A flag that we use to interpret key - whether it should be treated as Ctrl+ or not
bool bScrollBar = true;
CMyVirtualKeyboard *virtualKeyboard;
bool bSymFlag = false; // A flag that we use to check whether VK is shown


int main(int argc, char *argv[])
{
    // Let's search for programs first in our own 'app' folder
    QString oldPath = qgetenv("PATH");
    QString newPath = "app/native:"+oldPath;
    qputenv("PATH", newPath.toAscii().data());

    // We need to store our working folder somewhere, it is needed for ssh later
    int res = system ("echo -n `pwd`\"/data/\" > data/.myhome");
    if (res == -1){
    	fprintf(stderr, "Can't determine my home folder location, terminating");
    	exit(-1);
    }

    // We have a .profile included in the assets, let's copy it to the home folder
    QString appFolder(QDir::homePath());
    appFolder.chop(4);

    QString homeFolder = QDir::homePath();

    QString newFileName = homeFolder + "/.profile";
    QFile newFile(newFileName);
    QString originalFileName = appFolder + "app/native/.profile";
    QFile originalFile(originalFileName);

//    newFile.remove();
 	originalFile.copy(newFileName);


	// Init file descriptors opening proper devices
	masterFdG = ::open("/dev/ptyp1", O_RDWR);
	slaveFdG = ::open("/dev/ttyp1", O_RDWR | O_NOCTTY);
	// And here is where magic begins
    pidG_ = fork();
    if (pidG_ == 0) {
        dup2(slaveFdG, STDIN_FILENO);
        dup2(slaveFdG, STDOUT_FILENO);
        dup2(slaveFdG, STDERR_FILENO);

        // reset all signal handlers
        struct sigaction act;
        sigemptyset(&act.sa_mask);
        act.sa_handler = SIG_DFL;
        act.sa_flags = 0;
        for (int sig = 1; sig < NSIG; sig++)
          sigaction(sig, &act, 0L);

        // All we want in child process is to have sh running
        const char *arglist[] = {"/bin/sh", "-l", NULL};
        execvp(arglist[0], (char**)arglist);
    } else if (pidG_ == -1) {
        // fork() failed
  	    qDebug() << "fork() failed:";
        pidG_ = 0;
        return false;
    }
    // Drop garbage that is shown when you start the app
    char cGarbage[72];
    read(masterFdG, cGarbage, 72);

    // needed for correct QT initialization
    //qputenv("QT_QPA_FONTDIR", "/usr/fonts/font_repository/dejavu-ttf-2.17"); // needed for correct QT initialization

    qputenv("QT_QPA_PLATFORM_PLUGIN_PATH", "/usr/lib/qt4/plugins/platforms");

    //QCoreApplication::addLibraryPath("app/native/lib"); // blackberry plugin does not load without this line

    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QApplication app(argc, argv);

    mainWindow = new CMyMainWindow();
    QRect r = QApplication::desktop()->screenGeometry(0);
    mainWindow->resize(r.width()+1, r.height()+1);
    mainWindow->setStyleSheet("background-color: black; color: lightgray");

    console = new QTermWidget(1, mainWindow);

    font = QFont(QString("Monospace"), 6);
    font.setPixelSize(mainWindow->nFontSize);
    font.setStyle(QFont::StyleNormal);
    font.setWeight(QFont::Normal);
	console->setTerminalFont(font);

	console->setGeometry(0, 0, r.width()+2, r.height()-80);
	//console->setScrollBarPosition(QTermWidget::NoScrollBar);
    console->setScrollBarPosition(QTermWidget::ScrollBarRight);

    // Our 'soft buttons' menu
    Menu = new CMyMenu(mainWindow);
    Menu->MenuInit();

    // Our system menu
    SystemMenu = new CMySystemMenu(mainWindow);
    SystemMenu->MenuInit();

    QObject::connect(console, SIGNAL(finished()), mainWindow, SLOT(close()));

    mainWindow->show();

    virtualKeyboard = new CMyVirtualKeyboard(mainWindow);
    return app.exec();
}
