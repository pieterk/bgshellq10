/*
 * Copyright (C) 2012 BGmot <support@tm-k.com>
 */

#include <fcntl.h>
#include "mymenu.h"
#include "qtermwidget.h"

extern int masterFdG;
extern QTermWidget *console;
extern bool bCtrlFlag;

CMyMenu::CMyMenu(QWidget *parent) :
QWidget(parent)
{
}

// There is nothing to comment here, all button names tell you what they do
int CMyMenu::MenuInit(){
	btnCtrlC = new QToolButton(this);
    btnCtrlC->setObjectName(QString::fromUtf8("btnCtrlC"));
    btnCtrlC->setText(QString("Ctrl"));
    btnCtrlC->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

	btnTab = new QToolButton(this);
    btnTab->setObjectName(QString::fromUtf8("btnTab"));
    btnTab->setText(QString("Tab"));
    btnTab->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    btnLeft = new QToolButton(this);
    btnLeft->setObjectName(QString::fromUtf8("btnLeft"));
    btnLeft->setText(QString("←"));
    btnLeft->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    btnRight = new QToolButton(this);
    btnRight->setObjectName(QString::fromUtf8("btnRight"));
    btnRight->setText(QString("→"));
    btnRight->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    btnUp = new QToolButton(this);
    btnUp->setObjectName(QString::fromUtf8("btnUp"));
    btnUp->setText(QString("↑"));
    btnUp->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    btnDown = new QToolButton(this);
    btnDown->setObjectName(QString::fromUtf8("btnDown"));
    btnDown->setText(QString("↓"));
    btnDown->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    btnEsc = new QToolButton(this);
    btnEsc->setObjectName(QString::fromUtf8("btnEsc"));
    btnEsc->setText(QString("Esc"));
    btnEsc->setStyleSheet("QToolButton {background-color: gray; color:lightgray; } QToolButton:pressed { background-color: black; color: lightgray; }");

    QMetaObject::connectSlotsByName(this);
	SetGeometryPortrait();
	return 0;
}
void CMyMenu::SetGeometryPortrait(){
	QRect r = QApplication::desktop()->screenGeometry(0);
	int nW = r.width(); //720
	int nH = r.height(); //720
	int numberOfButtons = 7;
	int buttonSize = nW/numberOfButtons;
	int margin = 3;
	int heightOfBar = 80;


	this->setGeometry(0, nH-heightOfBar, nW, heightOfBar);
	btnCtrlC->setGeometry(QRect(0*buttonSize, 0, buttonSize+margin, heightOfBar));
	btnTab  ->setGeometry(QRect(1*buttonSize+margin, 0, buttonSize, heightOfBar));
	btnLeft ->setGeometry(QRect(2*buttonSize+margin, 0, buttonSize, heightOfBar));
	btnRight->setGeometry(QRect(3*buttonSize+margin, 0, buttonSize, heightOfBar));
	btnUp   ->setGeometry(QRect(4*buttonSize+margin, 0, buttonSize, heightOfBar));
	btnDown ->setGeometry(QRect(5*buttonSize+margin, 0, buttonSize, heightOfBar));
	btnEsc  ->setGeometry(QRect(6*buttonSize+margin, 0, buttonSize+margin, heightOfBar));
}
void CMyMenu::SetGeometryLandscape(){
	this->setGeometry(1206, 0, 73, 357);
    btnCtrlC->setGeometry(QRect(1, 1, 71, 51));
    btnTab->setGeometry(QRect(1, 52, 71, 51));
    btnLeft->setGeometry(QRect(1, 103, 71, 51));
    btnRight->setGeometry(QRect(1, 154, 71, 51));
    btnUp->setGeometry(QRect(1, 205, 71, 51));
    btnDown->setGeometry(QRect(1, 256, 71, 51));
    btnEsc->setGeometry(QRect(1, 307, 71, 50));
}

void CMyMenu::on_btnCtrlC_clicked(){
	// Now it is not Ctrl+C, it is CTRL+something, so let's wait for the next key or reset this flag
	if (bCtrlFlag == false){
		bCtrlFlag = true;
		btnCtrlC->setDown(true);
	}
	else{
		bCtrlFlag = false;
		btnCtrlC->setDown(false);
	}
	console->setFocus();
	return;
}
void CMyMenu::on_btnTab_clicked(){
	char c = 9;
	write(masterFdG, &c, 1);
	console->setFocus();
	return;
}
void CMyMenu::on_btnLeft_clicked(){
	char c[] = {0x1B,0x5B,'D'};
	write(masterFdG, c, 3);
	console->setFocus();
	return;
}
void CMyMenu::on_btnRight_clicked(){
	char c[] = {0x1B,0x5B,'C'};
	write(masterFdG, c, 3);
	console->setFocus();
	return;
}
void CMyMenu::on_btnUp_clicked(){
	char c[] = {0x1B,0x5B,'A'};
	write(masterFdG, c, 3);
	console->setFocus();
	return;
}
void CMyMenu::on_btnDown_clicked(){
	char c[] = {0x1B,0x5B,'B'};
	write(masterFdG, c, 3);
	console->setFocus();
	return;
}
void CMyMenu::on_btnEsc_clicked(){
	char c = 0x1B;
	write(masterFdG, &c, 1);
	console->setFocus();
	return;
}
