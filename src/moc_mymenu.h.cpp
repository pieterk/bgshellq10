/****************************************************************************
** Meta object code from reading C++ file 'mymenu.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.5)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mymenu.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mymenu.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.5. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_CMyMenu[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
       9,    8,    8,    8, 0x08,
      31,    8,    8,    8, 0x08,
      51,    8,    8,    8, 0x08,
      72,    8,    8,    8, 0x08,
      94,    8,    8,    8, 0x08,
     113,    8,    8,    8, 0x08,
     134,    8,    8,    8, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_CMyMenu[] = {
    "CMyMenu\0\0on_btnCtrlC_clicked()\0"
    "on_btnTab_clicked()\0on_btnLeft_clicked()\0"
    "on_btnRight_clicked()\0on_btnUp_clicked()\0"
    "on_btnDown_clicked()\0on_btnEsc_clicked()\0"
};

void CMyMenu::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        CMyMenu *_t = static_cast<CMyMenu *>(_o);
        switch (_id) {
        case 0: _t->on_btnCtrlC_clicked(); break;
        case 1: _t->on_btnTab_clicked(); break;
        case 2: _t->on_btnLeft_clicked(); break;
        case 3: _t->on_btnRight_clicked(); break;
        case 4: _t->on_btnUp_clicked(); break;
        case 5: _t->on_btnDown_clicked(); break;
        case 6: _t->on_btnEsc_clicked(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData CMyMenu::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject CMyMenu::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_CMyMenu,
      qt_meta_data_CMyMenu, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &CMyMenu::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *CMyMenu::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *CMyMenu::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_CMyMenu))
        return static_cast<void*>(const_cast< CMyMenu*>(this));
    return QWidget::qt_metacast(_clname);
}

int CMyMenu::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
